#include <math.h>
#include <opencv\cv.h>
#include <opencv\highgui.h>
#include <iostream>
#include <fstream>
#include <algorithm>

#define DRAW 1
#define MAZE 2
#define CATCH 3
#define QUIT 4
#define INBOUNDS(a,b,c) ((b < a) ? 0 : ((b > c) ? 0 : 1))
#define INBOUNDS2(a,b,c) ((b < a) ? a : ((b > c) ? c : b))

using namespace cv;
using namespace std;

//globs
int refresh=1,
	mode=0,
	edgeth,
	kernel,
	theta,
	handctr,
	vsize,
	c1fc,		//number of consecutive frames tip is in circle
	c2fc,
	c3fc,
	c4fc;

VideoCapture c(0);
RNG rng(12345);
Point index;
Point prevtip;
Mat orig,
	blurred,
	rotProc, //rotated image for processing
	rotMon,	//rotated image for monitoring
	canned, //binary edge image
	gs,
	canvas,
	out;

int key = waitKey(10),
	changemode=0,
	deadframecount = 0;

int scanLine(Mat *singleLine,Mat *cannedMaze, Point prev, Point cur)	//Maze routine, check if possible new path line passes through a wall/edge
{
	LineIterator l(*singleLine,prev,cur);
	int dx = cur.x - prev.x,
		dy = cur.y - prev.y;
	Point p = l.pos();
	if(dy / (double)dx > 0)
	{
		while(INBOUNDS(0,p.x,cur.x-1) && INBOUNDS(0,p.y, cur.y))
		{
			for(int x = INBOUNDS2(0,p.x-1,cannedMaze->cols); x <= INBOUNDS2(0,p.x,cannedMaze->cols); x++)
				for(int y = INBOUNDS2(0,p.y-1,cannedMaze->rows); y <= INBOUNDS2(0,p.y,cannedMaze->rows); y++)
					if(cannedMaze->at<uchar>(x,y))
						return 0;
			l.operator++();
			p = l.pos();
		}
	}
	else
	{
		while(INBOUNDS(1,p.x,cur.x-1) && INBOUNDS(1,p.y,cur.y-1))
		{
			for(int x = p.x-1; x <= p.x+1; x++)
				for(int y = p.y-1; y<=p.y-1;y++)
					if(cannedMaze->at<uchar>(x,y))
						return 0;
			l.operator++();
			p = l.pos();
		}
	}
	return 1;
}

void blackoutedges(Mat *victim)	//get rid of flickering edge pixels on edge of camera image
{
	for(int i = 0; i < victim->rows; i++)
	{
		victim->data[victim->step[0]*i + victim->channels() * 1 + 0]=0;
		victim->data[victim->step[0]*i + victim->channels() * 1 + 1]=0;
		victim->data[victim->step[0]*i + victim->channels() * 1 + 2]=0;
	}

	for(int j = 0; j < victim->cols; j++)
	{
		victim->data[victim->step[0]*1 + victim->channels() * j + 0]=0;
		victim->data[victim->step[0]*1 + victim->channels() * j + 1]=0;
		victim->data[victim->step[0]*1 + victim->channels() * j + 2]=0;
	}
}

void rotateImg(Mat *src, Mat *dst)			//rotate image by 180deg if special setup is used
{
	Point center(src->cols/2,src->rows/2);
	Mat r = getRotationMatrix2D(center,180,1);
	warpAffine(*src,*dst,r,Size(src->cols,src->rows));
}

double distance2(Point a, Point b)
{
	return sqrt((double)(b.x-a.x)*(b.x-a.x) + (b.y-a.y)*(b.y-a.y));
}

size_t invec(vector<Point> v, Point p)
{
	for(size_t i = 0; i < v.size(); i++)
		if(p.x == v[i].x && p.y == v[i].y)
			return i;
	return UINT_MAX;
}

void findorientation(vector<Point> pts, size_t idx,Mat *d)	//draw orientation (for testing's sake)
{
	if(INBOUNDS(0,idx+vsize,pts.size()) && INBOUNDS(0,idx-vsize,pts.size()))
	{
		Point k(pts[idx].x,pts[idx].y),
			kp(pts[idx+vsize].x,pts[idx+vsize].y),
			kn(pts[idx-vsize].x,pts[idx-vsize].y),
			mid((pts[idx+vsize].x+pts[idx-vsize].x)/2,(pts[idx+vsize].y+pts[idx-vsize].y)/2);
		line(*d,mid,k,Scalar(255,0,255));
		line(*d,kp,kn,Scalar(255,255,0));
	}
	else return;
}

Point majoritySuppression(vector<Point> possibleTips, vector<Point> hand, size_t range)	//lowest y
{
	size_t idx;
	Point m(orig.rows,orig.cols);
	for(size_t i = 0; i < possibleTips.size(); i++)
	{
		if(possibleTips[i].y < m.y)
			m = possibleTips[i];
	}
	return m;
}

Point computerefpt(vector<Point> handpts)	//compute reference point for hand point sorting
{
	vector<Point> ch;
	convexHull(handpts,ch);
	if(ch.size() < 3) return Point(0,0);
	Moments m = moments(ch);
	return Point(m.m10/m.m00,m.m01/m.m00);
}

bool less1(Point a, Point b, Point center)		//THIS SORTS CLOCKWISE...so complement it
{
	if(a.x - center.x >= 0 && b.x - center.x < 0)
		return true;
	if(a.x - center.x < 0 && b.x - center.x >= 0)
		return false;
	if(a.x - center.x == 0 && b.x - center.x == 0)
	{
		if(a.x - center.x >= 0 && b.x - center.x < 0)
			if(a.y - center.y >= 0 || b.y - center.y >=0)
				return a.y>b.y;
		return b.y>a.y;
	}

	//cross
	int determ = (a.x - center.x) * (b.y - center.y) - (b.x - center.x) * (a.y - center.y);
	if(determ<0)
		return true;
	if(determ>0)
		return false;

	int d1 = (a.x - center.x) * (a.x - center.x) + (a.y - center.y) * (a.y - center.y),
		d2 = (b.x - center.x) * (b.x - center.x) + (b.y - center.y) * (b.y - center.y);
	return d1 > d2;
}

vector<Point> merge(vector<Point> left, vector<Point> right, Point center)
{
	vector<Point> result;
	while(left.size() || right.size())
	{
		if(left.size() && right.size())
		{
			if(!less1(left.front(),right.front(),center))
			{
				result.push_back(left.front());
				left.erase(left.begin());
			}
			else
			{
				result.push_back(right.front());
				right.erase(right.begin());
			}
		}
		else if(left.size())
		{
			for(size_t i = 0; i < left.size(); i++)
				result.push_back(left[i]);
			break;
		}
		else if(right.size())
		{
			for(size_t i = 0; i < right.size(); i++)
				result.push_back(right[i]);
			break;
		}
	}
	return result;
}

vector<Point> mergesort(vector<Point> m)
{
	Point ref = computerefpt(m);
	if(m.size() <= 1)
		return m;
	vector<Point> left, right, result;
	size_t middle = (m.size() + 1) / 2;

	for(size_t i = 0; i < middle; i++)
		left.push_back(m[i]);
	for(size_t i = middle; i < m.size(); i++)
		right.push_back(m[i]);
	left = mergesort(left);
	right = mergesort(right);
	result = merge(left,right,ref);
	return result;
}

void trackbarHET(int pos, void *)
{
	refresh=1;
}
void trackbarHTA(int pos, void *)
{
	refresh=1;
}

void trackbarVS(int pos, void *)
{
	refresh=1;
}

size_t biggestctridx(vector<vector<Point>> contours)
{
	size_t maxidx = 0;
	for(size_t i = 0; i < contours.size(); i++)
	{
		if(contours[i].size() > contours[maxidx].size())
			maxidx=i;
	}
	return maxidx;
}

void outputhandcontour(vector<vector<Point>> contours,vector<Point> hand)
{
	Mat handm = Mat::zeros(orig.size(),CV_8UC3);

}

vector<Point> findbiggestcontour(vector<vector<Point>> contours)
{
	vector<Point> hand;
	vector<vector<Point>>::iterator i;
	int	curCon=0;
	handctr = -1;
	size_t maxsize = 0;
	for(i = contours.begin(); i!=contours.end(); i++)
	{
		if(i->size()>maxsize)
		{
			maxsize = i->size();
			handctr=curCon;
		}
		curCon++;
	}
	return contours[handctr];
}

void init_trackbars()
{
	edgeth=100;
	createTrackbar("Edge Threshold", "Canvas", &edgeth, 500, trackbarHET);
	theta=30;
	createTrackbar("Fingertip Angle Threshold","Canvas",&theta,90,trackbarHTA);
	vsize=30;
}

void printptstofile(vector<Point> pts)
{
	ofstream p,
		     p2;
	p.open("possibleIndexPts.txt");
	p2.open("possibleMidPts.txt");
	for(size_t i = 0; i < pts.size(); i++)
		p << pts[i] << endl;
	p.close();
}

void init_windows()
{
	//canvas window
	namedWindow("Canvas",CV_WINDOW_AUTOSIZE);
	moveWindow("Canvas",100,100);
	//monitor window
	namedWindow("Webcam Monitor",CV_WINDOW_AUTOSIZE);
}

int drawTestCircles(Mat *dst, Point tip, int prevmode, int nomaze)		//draw circles at predefined areas for ONE tip detection testing
{
	int dtc1 = distance2(tip,Point(3*dst->cols / 4,dst->rows / 4)),		//top right
		dtc3 = distance2(tip,Point(3*dst->cols / 4,3*dst->rows / 4)),		//bot right
		dtc4 = distance2(tip,Point(dst->cols / 4,dst->rows / 4));		//top left
	putText(*dst,"Draw",Point(dst->cols / 4 - 25,dst->rows / 4),FONT_HERSHEY_COMPLEX_SMALL,.8,Scalar(255,255,0),1,CV_AA);
	putText(*dst,"Maze",Point(3*dst->cols / 4 - 25,dst->rows / 4),FONT_HERSHEY_COMPLEX_SMALL,.8,Scalar(255,255,0),1,CV_AA);
	putText(*dst,"Exit",Point(3*dst->cols / 4 - 30,3*dst->rows / 4),FONT_HERSHEY_COMPLEX_SMALL,.8,Scalar(255,255,0),1,CV_AA);

	if(dtc1<50)
	{
		if(!nomaze)
		{
			c2fc++;
			circle(*dst,Point(3*dst->cols / 4,dst->rows / 4),50,Scalar(0,(c2fc/25.0)*255.0,0),3);
		}
		else
			circle(*dst,Point(3*dst->cols / 4,dst->rows / 4),50,Scalar(0,0,255),3);
	}
	else
	{
		circle(*dst,Point(3*dst->cols / 4,dst->rows / 4),50,Scalar(255,255,255),3);
		c2fc=0;
	}

	if(dtc3<50)
	{
		c4fc++;
		circle(*dst,Point(3*dst->cols / 4,3*dst->rows / 4),50,Scalar(0,(c4fc/25.0)*255.0,0),3);
	}
	else
	{
		circle(*dst,Point(3*dst->cols / 4,3*dst->rows / 4),50,Scalar(255,255,255),3);
		c4fc=0;
	}

	if(dtc4<50)
	{
		c1fc++;
		circle(*dst,Point(dst->cols / 4,dst->rows / 4),50,Scalar(0,(c1fc/25.0)*255.0,0),3);
	}
	else
	{
		circle(*dst,Point(dst->cols / 4,dst->rows / 4),50,Scalar(255,255,255),3);
		c1fc=0;
	}

	if(c1fc==25)
	{
		c1fc=0;
		return DRAW;
	}
	else if(c2fc==25)
	{
		c2fc=0;
		return MAZE;
	}
	else if(c4fc==25)
	{
		c4fc=0;
		return QUIT;
	}
	return -1;	//stay in menu
}

Point findTips(vector<Point> hand, int threshAngle, int vecsize, Mat *drawing, int showorientation)
{
	ofstream p,
		     p2;
	p.open("possibleIndexPts.txt");
	p2.open("possibleMidPts.txt");
	size_t n = (size_t)vecsize;
	hand = mergesort(hand);
	Point kpn,
		  kmn;
	vector<Point> tips,
				  indexpts,
				  thumbpts,
				  midpts,
				  ringpts,
				  pinkypts;
	vector<vector<Point>> fingers2;
	Point index(0,0);
	double angle = 0,
		   dot,
		   magP,
		   magN;
	size_t ind_idx = 0;
	int notipct = 0,
		curFinger = -1,
		findind = 1;
	vector<Point> fingers[] = {indexpts,midpts,ringpts,pinkypts};
	if(hand.size() > n) 	//don't bother figuring out if it's a tip if either the i+-n indices are out of bounds
	{
		for(size_t i = n; i < hand.size()-n; i++)
		{
			if((i+n > hand.size() || i-n < 0)) 
			{
				findind=0;
				break;
			}
			kpn = Point(hand[i-n].x,hand[i-n].y); 
			kmn = Point(hand[i+n].x,hand[i+n].y);
			Point vecP(hand[i+n].x - hand[i].x,hand[i+n].y - hand[i].y),		//This is the vector from p[k] to p[k+n] (yes, I know it's a Point type)
				  vecN(hand[i-n].x - hand[i].x,hand[i-n].y - hand[i].y);			//	"  "  "  "  p[k] to p[k-n]
			dot = vecP.x * vecN.x + vecP.y * vecN.y;							//(p[i+n] - p[i]) dot (p[i-n] - p[i])
			magP = sqrt((double)vecP.x*vecP.x + vecP.y*vecP.y);
			magN = sqrt((double)vecN.x*vecN.x + vecN.y*vecN.y);
			angle = acos(dot/(magP*magN)) * (180/CV_PI);
			if(hand[i].y < kpn.y || hand[i].y < kmn.y)
			{
				if(!((hand[i].x >= kpn.x && hand[i].x >= kmn.x) || (hand[i].x <= kpn.x && hand[i].x <= kmn.x)))
				{
					if(angle<=threshAngle)
						tips.push_back(hand[i]);
				}
			}
		}

		if(tips.size())
		{
			index = majoritySuppression(tips,hand,0);
			ind_idx = invec(hand,index);
		}
		else
			index = majoritySuppression(hand,hand,0);	//if tip finding goes bad because there's not enough points for P[i+n], just take the lowest y-coord point as the tip
	}
	if(showorientation)
		findorientation(hand,ind_idx,drawing);
	return index;
}

void drawroutine(Mat *dst)
{
	index = Point(0,0);
	prevtip = Point(0,0);
	cout << "Entering drawing mode" << endl;
	cout << "Press ESC to return to menu" << endl;
	cout << "Press \'c\' to clear the picture" << endl;
	int key = waitKey(10);
	Mat drawing = Mat::zeros( gs.size(), CV_8UC3 );
	while(key!=27)
	{
		switch(key)
		{
		case 'c': drawing = Mat::zeros(rotMon.size(),CV_8UC3);
			break;
		case 'o': if(!imwrite("drawing.jpg",drawing)) cout << "File write failed" << endl;
				  break;
		}
		vector<vector<Point> > contours,
							   extractedhand;
		vector<Point>		   fingertipPoints,
							   handContour;
		c>>*dst;
		rotateImg(dst,&rotMon);
		rotateImg(dst,&rotProc);
		cvtColor(rotProc,gs,CV_BGR2GRAY);
		Canny(gs,canned,edgeth+1,(1+edgeth)*3);	
		vector<Vec4i> hierarchy;
		blackoutedges(&canned);
		findContours(canned,contours,hierarchy,CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
		canvas = Mat::zeros(gs.size(),CV_8UC3);
		for( int i = 0; i < (int)contours.size(); i++ )
		{
			Scalar color = Scalar( 0,0,255 );
			drawContours( canvas, contours, i, color);
		}
		if(contours.size())							
		{										
			handContour = findbiggestcontour(contours);
			extractedhand.push_back(handContour);
			if(handContour.size())
			{
				prevtip = index;
				index = findTips(handContour,theta,vsize,&drawing,0);	
				if(distance2(prevtip,index) > 5 && (index.x ==0 && index.y == 0)) 
				{
					index = prevtip;	//if there's a big jump of a tip from frame to frame, reuse the old one
					deadframecount++;
				}
				circle(canvas,index,5,Scalar(0,255,255),-1);
				circle(drawing,index,1,Scalar(0,255,255),-1);
				line(drawing,prevtip,index,Scalar(255,255,255));
			}
		}
		imshow("Canvas",drawing);
		imshow("Webcam Monitor",rotMon);
		key = waitKey(10);
	}

}

int checkStartPos(Point p, Mat *cannedMaze)
{
	double distance = distance2(p,Point(cannedMaze->cols / 2 + 19,cannedMaze->rows - 19));
	if(distance < 15)
		return 1;
	return 0;
}

int checkEndPos(Point p, Mat *cannedMaze)
{
	double distance = distance2(p,Point(cannedMaze->cols / 2 - 19, 19));
	if(distance < 15)
		return 1;
	return 0;
}

void mazeroutine(Mat *maze, Mat *dst)
{
	int isAtStart = 0;
	index = Point(0,0);
	prevtip = Point(0,0);
	cout << "Press ESC to return to menu" << endl;
	cout << "Hit the \"o\" key to write the drawing to file" << endl;
	int key = waitKey(10);
	Mat drawing = maze->clone(),
		cannedMaze = drawing.clone(),
		singleline(drawing.size(),CV_8UC3);
	Canny(*maze,cannedMaze,edgeth+1,(1+edgeth)*3);
	while(key!=27)
	{
		vector<vector<Point> > contours,
							   extractedhand;
		vector<Point>		   fingertipPoints,
							   handContour;
		c>>*dst;
		rotateImg(dst,&rotMon);
		rotateImg(dst,&rotProc);
		cvtColor(rotProc,gs,CV_BGR2GRAY);
		Canny(gs,canned,edgeth+1,(1+edgeth)*3);	
		vector<Vec4i> hierarchy;
		blackoutedges(&canned);
		findContours(canned,contours,hierarchy,CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
		canvas = Mat::zeros(gs.size(),CV_8UC3);
		for( int i = 0; i < (int)contours.size(); i++ )
		{
			Scalar color = Scalar( 0,0,255 );
			drawContours( canvas, contours, i, color);
		}
		if(contours.size())							
		{										
			handContour = findbiggestcontour(contours);
			extractedhand.push_back(handContour);
			if(handContour.size())
			{
				prevtip = index;
				index = findTips(handContour,theta,vsize,&drawing,0);	
				if(distance2(prevtip,index) > 3 && (index.x ==0 && index.y == 0)) 
				{
					index = prevtip;	//if there's a big jump of a tip from frame to frame, reuse the old one
					deadframecount++;
				}
				if(!isAtStart)
					isAtStart = checkStartPos(index,maze);
				if(isAtStart)
				{
					line(singleline,prevtip,index,Scalar(0,0,255));
					if(scanLine(&singleline,&cannedMaze,prevtip,index))
					{
						circle(canvas,index,5,Scalar(0,255,255),-1);
						circle(drawing,index,1,Scalar(255,0,0),-1);
						line(drawing,prevtip,index,Scalar(0,0,255));
						circle(drawing,Point(cannedMaze.cols / 2 + 19,cannedMaze.rows - 19),15,Scalar(0,255,0));
						if(checkEndPos(index,&cannedMaze))
							return;
					}
				}
			}
		}
		if(!isAtStart)
		{
			circle(drawing,Point(cannedMaze.cols / 2 + 19,cannedMaze.rows - 19),15,Scalar(0,0,255));
			circle(rotMon,Point(rotMon.cols / 2 + 19,rotMon.rows - 19),15,Scalar(0,0,255));
		}
		imshow("Canvas",drawing);
		imshow("Webcam Monitor",rotMon);
		key = waitKey(10);
	}
}

void catchroutine(Mat *dst)
{
		index = Point(0,0);
	prevtip = Point(0,0);
	cout << "Press ESC to return to menu" << endl;
	int key = waitKey(10);
	Mat drawing = Mat::zeros( gs.size(), CV_8UC3 );
	while(key!=27)
	{
		switch(key)
		{
		case 'c': drawing = Mat::zeros(rotMon.size(),CV_8UC3);
			break;
		}
		vector<vector<Point> > contours,
							   extractedhand;
		vector<Point>		   fingertipPoints,
							   handContour;
		c>>*dst;
		rotateImg(dst,&rotMon);
		rotateImg(dst,&rotProc);
		cvtColor(rotProc,gs,CV_BGR2GRAY);
		Canny(gs,canned,edgeth+1,(1+edgeth)*3);	
		vector<Vec4i> hierarchy;
		blackoutedges(&canned);
		findContours(canned,contours,hierarchy,CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
		canvas = Mat::zeros(gs.size(),CV_8UC3);
		for( int i = 0; i < (int)contours.size(); i++ )
		{
			Scalar color = Scalar( 0,0,255 );
			drawContours( canvas, contours, i, color);
		}
		if(contours.size())							
		{										
			handContour = findbiggestcontour(contours);
			extractedhand.push_back(handContour);
			if(handContour.size())
			{
				prevtip = index;
				index = findTips(handContour,theta,vsize,&drawing,0);	
				if(distance2(prevtip,index) > 5 && (index.x ==0 && index.y == 0)) 
				{
					index = prevtip;	//if there's a big jump of a tip from frame to frame, reuse the old one
					deadframecount++;
				}
				circle(canvas,index,5,Scalar(0,255,255),-1);
				circle(drawing,index,1,Scalar(0,255,255),-1);
				line(drawing,prevtip,index,Scalar(255,255,255));
			}
		}
		imshow("Canvas",drawing);
		imshow("Webcam Monitor",rotMon);
		key = waitKey(10);
	}
}

void help()
{
	cout << "Here's how to use this program (Main Menu Mode): " << endl;
	cout << "\t-Bring your finger to the \"Draw\" circle if you want to draw stuff" << endl;
	cout << "\t-Bring your finger to the \"Maze\" circle to navigate through a maze" << endl;
	cout << "\t-Bring your finger to the \"Exit\" circle to kill the program" << endl;
	cout << "\t-Adjust the \"Edge Threshold\" and \"Finger Angle Threshold\" as needed to fit your hand" << endl;
	cout << "\t-Hitting \'o\' will output the hand contour with the labeled fingertip to \"handcontour.jpg\"" << endl;
	cout << "\t-Hitting \'s\' will enable showing finger orientation on the canvas window" << endl;
	cout << "\t-Hitting \'q\' writes the current canvas (main menu only) screen to \"canv.jpg\"" << endl;
	cout << "\t-Hitting \'c\' writes the binary edge image to \"canned.jpg\"" << endl;
	cout << "\t-Hitting \'e\' writes the webcam monitoring image to \"cam.jpg\"" << endl;
	cout << "\t-Finally, hitting escape or navigating to the Exit circle kills the program" << endl;
}

int main(int argc, char **argv)
{
	int disablemazemode = 0,
		showorientation = 0,
		outputhand=0;
	Mat maze;
	if(argc<2)
	{
		cout <<  "Disabling Maze Mode...." << endl;
		cout <<  "How to run with maze: program <nameofMazeFile>" << endl;
		disablemazemode=1;
	}
	else
		maze = imread(argv[1],CV_LOAD_IMAGE_COLOR);
	if(maze.empty() && disablemazemode==0)
	{
		cout <<  "Disabling Maze Mode...." << endl;
		cout <<  "File not found/bad file" << endl;
		disablemazemode=1;
	}
	if(c.isOpened())
	{
		c>>orig;
		cvMoveWindow("Webcam Monitor",250,250);
	}
	else
	{
		printf("Camera load failed\n");
		exit(0);
	}

	init_windows();
	init_trackbars();
	c1fc=0;
	c2fc=0;
	c3fc=0;
	c4fc=0;
	help();
	while(key!=27)	//kb loop
	{
		switch(key)
		{
		    case 'o': 
					  outputhand=1;
					  break;
			case 'h': help();
					  break;
			case 's': if(!showorientation) showorientation=1;
					  else showorientation=0;
					  break;
			case 'c':if(!imwrite("canned.jpg",canned)) cout << "Uh oh, file write failed..." << endl;
					 break;
			case 'e': if(!imwrite("cam.jpg",rotMon)) cout << "Uh oh, file write failed..." << endl;
					  break;

		}
		vector<vector<Point> > contours,
							   extractedhand;
		vector<Point>		   fingertipPoints,
							   handContour;
		c>>orig;
		rotateImg(&orig,&rotMon);
		rotateImg(&orig,&rotProc);
		cvtColor(rotProc,gs,CV_BGR2GRAY);
		Canny(gs,canned,edgeth+1,(1+edgeth)*3);	
		vector<Vec4i> hierarchy;
		blackoutedges(&canned);
		findContours(canned,contours,hierarchy,CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
		Mat drawing = Mat::zeros( gs.size(), CV_8UC3 );
		out = Mat::zeros(gs.size(),CV_8UC3);
		canvas = Mat::zeros(gs.size(),CV_8UC3);
		for( int i = 0; i < (int)contours.size(); i++ )
		{
			Scalar color = Scalar( 0,0,255 );
			drawContours( drawing, contours, i, color);
			drawContours( out, contours, i, color);
		}
		if(deadframecount > 20)		//if too many dead frames/same tip point, reset to (0,0)
		{
			prevtip = index = Point(0,0);
			deadframecount=0;
		}

		if(contours.size())							
		{										
			handContour = findbiggestcontour(contours);
			extractedhand.push_back(handContour);
			if(handContour.size())
			{
				prevtip = index;
				index = findTips(handContour,theta,vsize,&drawing,showorientation);	
				if(distance2(prevtip,index) > 3 && (index.x ==0 && index.y == 0)) 
				{
					index = prevtip;	//if there's a big jump of a tip from frame to frame, reuse the old one
					deadframecount++;
				}
				circle(drawing,index,5,Scalar(0,255,255),-1);
				circle(rotMon,index,5,Scalar(0,255,255),-1);
				circle(out,index,5,Scalar(0,255,255),-1);
				if(outputhand)
				{
					if(!imwrite("handcontour.jpg",out)) cout << "Uh oh, file write failed..." << endl;
					outputhand=0;
				}
			}
		}
		else
			deadframecount++;

		mode = drawTestCircles(&drawing, index, mode, disablemazemode);
		switch(mode)
		{
			case DRAW:drawroutine(&drawing);
				index = Point(0,0);
				cout << "Returning to main menu..." << endl;
				break;
			case MAZE:
				if(!disablemazemode)
					mazeroutine(&maze,&drawing);
				cout << "Returning to main menu..." << endl;
				index = Point(0,0);
				break;
			case QUIT:
				return 0;
			case -1:
				break;
		}
		imshow("Canvas", drawing);
		imshow("Webcam Monitor",rotMon);
		key=waitKey(10);

	}
	return 0;

}

